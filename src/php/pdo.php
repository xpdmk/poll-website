<?php
header('Content-Type: application/json');
//IF limit is not set, use 1
$limit = isset($_GET['howMany']) ? $_GET['howMany'] : 1;

//"GETILLÄ HAETAAN, POSTILLA LAITETAAN"

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	if (isset($_GET['category']))
	{
	
		if (isset($_GET['pollId'])) {

			$id = preg_replace('/\s\s+/', '', $_GET['pollId']); //strips all whitespace characters
			$id = htmlspecialchars($id);
	        
	        
			$availableIDS = [];
			$polls = getAvailablePollIDS();
			
			for ($i = 0; $i < sizeof(getAvailablePollIDS()); $i++) {
				 $availableIDS[] = $polls[$i]->p_id;
			}
			
			if (!in_array($id, $availableIDS)) {
				$errorJSON = json_encode(array('No poll found with given ID'));
				echo $errorJSON;
			} else {
				$poll[] = getPollByID($id);
				
		        echo resultToJSON($poll);
			}
		}


		
//	    $offset = isset($_GET['pageNumber']) ? ($_GET['pageNumber'] * 5) : 5;
	    
	    //VIEW EVERY VOTE, also show un-voted replyOptions (v_date NULL) 
		$select = 'SELECT Polls.p_id, Polls.p_date, Polls.question, 
		ReplyOptions.replyOption, ReplyOptions.o_id, Votes.v_date
		FROM ReplyOptions
		INNER JOIN Polls
		ON ReplyOptions.poll_id=Polls.p_id
		LEFT JOIN Votes
		ON Votes.option_id=ReplyOptions.o_id;';
			
		//	"ORDER BY ?, question, o_id;'; 
			
		$category = htmlspecialchars($_GET['category']);
		
		
		switch(strtolower($category)) 
		{
			case "popular":
				getPopularPolls($limit);
				
				break;
			case "newest":
				
				getNewestPolls($limit);

				break;
				
			case "random":
				
				getRandomPolls($limit);

        } // END SWITCH 
	} else 
	{
		echo "Invalid get";
	} 
}//  END ($_SERVER['REQUEST_METHOD'] === 'GET')

if ($_SERVER['REQUEST_METHOD'] === 'POST') 
{
	 // The request is using the POST method

	// VOTE ON EXISTING POLL
	if (isset($_POST['vote']) && isset($_POST['pollId'])) 
	{
		$replyOption[] = htmlspecialchars(($_POST['vote']));
		$pollID = htmlspecialchars(($_POST['pollId']));
		
		
		//VOTE ON POLL
		$vote = 'INSERT INTO Votes (option_id) VALUES (?);'; // 1= ReplyOptions.o_id = Votes.option_id
		queryDB($vote, $replyOption);
		
		//return updated data for poll
		$getPollByOptID = 'SELECT Polls.p_id, Polls.p_date, Polls.question, 
		ReplyOptions.replyOption, ReplyOptions.o_id, Votes.v_date
		FROM ReplyOptions
		INNER JOIN Polls
		ON ReplyOptions.poll_id=Polls.p_id
		LEFT JOIN Votes
		ON Votes.option_id=ReplyOptions.o_id
		WHERE p_id=?
		ORDER BY question, o_id;';
	
        $polls[] = getPollByID($pollID);
        
        echo resultToJSON($polls);
		
	} else if (isset($_POST['question']) && isset($_POST['options'])) 
	{
		$question = htmlspecialchars(($_REQUEST['question']));
		$options = $_REQUEST['options']; 
		
		createNewPoll($question, $options);
		
	} else {
		echo "Invalid request";
	}
	

	}// END if http-type post

	


function getPopularPolls($limit){
	//LIST ALL POLL_ID:S WITH VOTECOUNTS; order from most to least popular 

	$selectPopular = "SELECT COUNT(Votes.v_id) AS voteCount,Polls.p_id 
	FROM Polls 
	LEFT JOIN ReplyOptions         
	ON ReplyOptions.poll_id=Polls.p_id         
	LEFT JOIN Votes       
	ON Votes.option_id=ReplyOptions.o_id
	GROUP BY Polls.p_id
	ORDER BY voteCount DESC;";

	$popularPolls = queryDB($selectPopular, NULL);
	
	// USE sizeof(polls) as $limit if we have less polls than limit
	$limit = ($limit <= sizeof($popularPolls)) ? $limit : sizeof($popularPolls);
	
    $result = [];
    
    //query for $limit number of polls w/ their id
    for($i = 0; $i < $limit; $i++) {
        $result[] = getPollByID($popularPolls[$i]->p_id);
    }
	echo resultToJSON($result);
}

function getNewestPolls($limit){
	
	$selectNewest = "SELECT p_id FROM Polls ORDER BY p_date DESC;";
	
	$ids = queryDB($selectNewest, NULL);
	

	
	// USE sizeof(polls) as $limit if we have less polls than limit
	$limit = ($limit <= sizeof($ids)) ? $limit : sizeof($ids);
	
	$result = [];
	
	//query for $limit number of polls w/ their id
	for($i = 0; $i<$limit; $i++){
		$result[] = getPollByID($ids[$i]->p_id);
	}
	echo resultToJSON($result);
		
	
}

function getRandomPolls($limit){
    $ids = getAvailablePollIDS();
    
	shuffle($ids);                
	shuffle($ids);                

    $randomPoll = [];
	
	// USE sizeof(polls) as $limit if we have less polls than limit
	$limit = ($limit <= sizeof($ids)) ? $limit : sizeof($ids);
	
	for($i = 0; $i<$limit; $i++)
	{
		$id = $ids[$i]->p_id;
    	$randomPoll[] = getPollByID($id);
	}
	echo resultToJSON($randomPoll);
}

function getAvailablePollIDS(){
	$selectNewest = "SELECT p_id FROM Polls ORDER BY p_date DESC;";

    $ids = queryDB($selectNewest, NULL);

	return $ids;
}

function createNewPoll($question, $options) {
	//CREATE NEW POLL
		if (strlen($question) > 0 && sizeof($options) >= 2 && !in_array("", $options)) {
			$createPoll = 'INSERT INTO Polls (question) VALUES (?);'; // 1 = question
			$createOption = 'INSERT INTO ReplyOptions (replyOption, poll_id) SELECT ?, Polls.p_id FROM Polls WHERE (Polls.question=?) ORDER BY p_date DESC LIMIT 1;'; // for each option; 1= replyoption, 2= poll question    
		
			queryDB($createPoll, [$question]);
	
			foreach ($options as $opt) {
				if (strlen($opt) > 0) //IGNORE EMPTY OPTIONS
				{
					$params = array(htmlspecialchars($opt), $question);
					queryDB($createOption, $params);
					
				}
			}
			
			//return data for new poll
			$getCreatedPoll = 'SELECT Polls.p_id, Polls.p_date, Polls.question, 
			ReplyOptions.replyOption, ReplyOptions.o_id, Votes.v_date
			FROM ReplyOptions
			INNER JOIN Polls
			ON ReplyOptions.poll_id=Polls.p_id
			LEFT JOIN Votes
			ON Votes.option_id=ReplyOptions.o_id
			WHERE question=?
			ORDER BY p_date DESC, question, o_id
			LIMIT 1;';
		
	        $polls[] = queryDB($getCreatedPoll, [$question]);
	        
	        echo resultToJSON($polls);
	} 
}//END ADD NEW POLL



function queryDB($statement, $parameters) 
{
	try{
		// Create a new connection.
		// You'll probably want to replace hostname with localhost in the first parameter.
		// Note how we declare the charset to be utf8mb4.  This alerts the connection that we'll be passing UTF-8 data.  This may not be required depending on your configuration, but it'll save you headaches down the road if you're trying to store Unicode strings in your database.  See "Gotchas".
		// The PDO options we pass do the following:
		// \PDO::ATTR_ERRMODE enables exceptions for errors.  This is optional but can be handy.
		// \PDO::ATTR_PERSISTENT disables persistent connections, which can cause concurrency issues in certain cases.  See "Gotchas".
	
		$ip = getenv('IP');
		$username = getenv('C9_USER');
		$password = "";
		$database = "c9";
	
		$link = new \PDO(  "mysql:host=$ip; dbname=$database; charset=utf8",
							$username,
							$password,
							array(
								
								\PDO::ATTR_EMULATE_PREPARES => false,
								\PDO::ATTR_ERRMODE => \PDO::ERRMODE_WARNING,
								\PDO::ATTR_PERSISTENT => true,
								\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") 
						);
	 
		//    $handle = $link->prepare('select Username from Users where UserId = ? or Username = ? limit ?');
	
		$handle = $link->prepare($statement);
		// PHP bug: if you don't specify PDO::PARAM_INT, PDO may enclose the argument in quotes.  This can mess up some MySQL queries that don't expect integers to be quoted.
		// See: https://bugs.php.net/bug.php?id=44639
		// If you're not sure whether the value you're passing is an integer, use the is_int() function.
		// (This bug was fixed in Oct. 2016, but the fix is not applied to the version of PHP used in this document; see https://bugs.php.net/bug.php?id=73234)
	


		$result = null;
		
		$queryType = explode(" ", $statement)[0];
		
		if (strcasecmp($queryType, "SELECT") == 0)  //Case-insensitive String comparison; 0 = strings match 
		{
			$handle->execute($parameters);
			$result = $handle->fetchAll(\PDO::FETCH_OBJ);               
		} else 
		{
			$result = $handle->execute($parameters);
		}

	 
		// Using the fetchAll() method might be too resource-heavy if you're selecting a truly massive amount of rows.
		// If that's the case, you can use the fetch() method and loop through each result row one by one.
		// You can also return arrays and other things instead of objects.  See the PDO documentation for details.

		return $result;
		
	} catch(\PDOException $e)
	{
		echo("// EXCEPTION // MESSAGE".$e->getMessage()."  TRACE: ".$e->getTrace()."   ");
	}    
		
}


function getPollByID($id)
{
	// get poll w/ id; ? = id
	$getPollByID = 'SELECT Polls.p_id, Polls.p_date, Polls.question, 
	ReplyOptions.replyOption, ReplyOptions.o_id, Votes.v_date
	FROM ReplyOptions
	INNER JOIN Polls
	ON ReplyOptions.poll_id=Polls.p_id
	LEFT JOIN Votes
	ON Votes.option_id=ReplyOptions.o_id
	WHERE p_id=?
	ORDER BY question, o_id;';
	
	$queryID[] = $id;
	$result = queryDB($getPollByID, $queryID);
	
	return $result;
}


function resultToJSON($result) 
{

// $result is array of one or more objects  [{}]
$allPolls = array();

foreach ($result as $row) 
{
    $allPolls[] = handleSingleResult($row);
}

	$result = json_encode($allPolls);
	return $result;
}
	
function handleSingleResult($row) 
{

$singlePollArray = null;
$prevRow = null;
$currRow = null;
$currOption = null;

for ($i = 0; $i < sizeof($row); $i++ )
{
	$currRow = $row[$i];

	if ($i == 0) {
		$singlePollArray = array(
			'id' => $currRow->p_id,
			'date' => $currRow->p_date,
			'question' => $currRow->question,
			'voteCount' => 0,
			'replyOptions' => array()
		);
	}

		//Jos on eka rivi; tai Jos edellisellä rivillä uusi replyOption-id, lisätään uusi array seuraavalle vastausvaihtoehdolle
		if( !isset($prevRow) || $currRow->o_id != $prevRow->o_id) {

			//Jos on eka rivi, ei vielä ole mitään dataa työnnettävissä
			if($i != 0) {
				$singlePollArray['voteCount'] += $currOption['voteCount'];
				array_push($singlePollArray['replyOptions'], $currOption); //push previous row to array
			}
			
			 $currOption = array( //init array for new replyOption
				'id' => $currRow->o_id,
				'replyOption' => $currRow->replyOption,
				'voteCount' => isset($currRow->v_date) ? 1 : 0,
				'voteDates' => isset($currRow->v_date) ? array($currRow->v_date) : []
				);
		} else {
			$currOption['voteCount']++;
			array_push($currOption['voteDates'], $currRow->v_date);


		}
		$prevRow = $currRow;

		//Työnnetään hakutuloksen viimeinen arvo viimeisellä iteraatiolla                
		if ($i == sizeof($row)-1 ){
			
			if (!in_array($singlePollArray['replyOptions'], $currOption)) 
			{ 					
				$singlePollArray['voteCount'] += $currOption['voteCount'];
				array_push($singlePollArray['replyOptions'], $currOption); 
			}
		}

	}

    return $singlePollArray;
	    
}


?>