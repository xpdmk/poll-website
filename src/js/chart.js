// insertPollBlock({
// 	containerQuery: "popular", 
// 	requestParameters: {category: "popular"}, 
// 	options: {showCategory: true}
//insertPoll({category: "random", howMany: 5, currPage: 1});
'use strict';

/**
 * Constructs a poll block with parameters and inserts
 * it to a container with same id as category name
 * @param parameters - Javascript object with fields:
 *		category: String that indicates the element id 
 *		the poll block should be inserted in and what 
 *		order JSONs should be sent back 
 * 
 *		howMany: Integer for limiting the amount of JSONs
 *		should be sent back
 * 
 *		currPage (not implemented yet): Integer that
 *		indicates what page requesting view is in. For
 *		example: It would be 1 when on the first page. If
 *		user would like to see more polls in that category,
 *		the view would send a request, where this parameter
 *		would be 2, to the server.
**/
function insertPoll(parameters) {
	
	function handleResponse(responseText) {
		if (responseText[0] == "No poll found with given ID") {
			console.log("no result"); // TODO: Fix text when no search results
			var searchContainer = document.getElementById('search-container');
			var resultPlaceholder = document.createElement('p');
			resultPlaceholder.innerHTML = "No matches found with search input";
			searchContainer.appendChild(resultPlaceholder);
			
		} else if (Array.isArray(responseText)) {
			for (var i = 0; i < responseText.length; i++) {
				createPollBlock(responseText[i], parameters.category);
			}
		} else {
			createPollBlock(responseText[0], parameters.category);
		}
	}
	
	requestJSON(parameters, handleResponse);
}
/**
 * Request JSON
**/
function requestJSON(parameters, callback) {
	// Requests JSON from server
	$.ajax({
		method: "GET",
		url: "php/pdo.php",
		dataType: "json",
		data: parameters,
		success: callback
	});
}
/**
 * Constructs poll block from JSON values
 * @param response - JSON containing the data of the poll
 * @param category - Element id of the container poll block should be inserted in
**/
function createPollBlock(response, category) {
	var poll = response;

	var container = document.getElementById(category);


	var pollContainer = document.createElement("div");
	pollContainer.setAttribute("class", "poll-container");
	
	var containersAlreadyIn = container.children;
	var place = ""+containersAlreadyIn.length;
	
	var pollDetails = document.createElement("p");
	pollDetails.setAttribute("class", "poll-details");
	pollDetails.innerHTML = "Poll ID: " + poll.id + "<br>Vote count: " + poll.voteCount + "<br>Created: " + poll.date;
	pollContainer.appendChild(pollDetails);
	
	// Header for poll question
	var questionTitle = document.createElement("h3");
	//questionTitle.setAttribute("class", "left");
	questionTitle.textContent = poll.question;
	pollContainer.appendChild(questionTitle);

	// Creates Results-, History- and Vote tabs 
	
	// List for tabs
	var tabList = document.createElement("ul");
	tabList.setAttribute("class", "nav nav-tabs");

//	var tabNames = ["Results", "History"];
	
	var tabNames = ["Results", "History", "Answer"];
	for (var i = 0; i < tabNames.length; i++) {
		var listItem = document.createElement("li");
		var hyperLink = document.createElement("a");
		
		if (i == 0) {
		  // Sets class="active" for Results-tab
			listItem.setAttribute("class", "active");
			hyperLink.setAttribute("aria-expanded", true);
			
		}
		listItem.setAttribute("class", listItem.className+" "+tabNames[i].toLowerCase());
		hyperLink.setAttribute("data-toggle", "tab");
		hyperLink.setAttribute("href", "#" + category + tabNames[i].toLowerCase() + place+"_pollID_"+poll.id);
		hyperLink.textContent = tabNames[i];
		
		listItem.appendChild(hyperLink);
		tabList.appendChild(listItem);
	}
	pollContainer.appendChild(tabList);
	
	// Div for contents of tabs
	var tabContent = document.createElement("div");
	tabContent.setAttribute("class", "tab-content");
	
	
	//// Results
	// Div for Results-tab content
	var results = document.createElement("div");
	var resultsId = category+"results"+place+"_pollID_"+poll.id;
	results.setAttribute("id", resultsId);
	results.setAttribute("class", "tab-pane fade in active");
	
	// Div inside Results-tab
	var resultChartContainer = document.createElement("div");
	resultChartContainer.setAttribute("class", "result-chart-container");
	
	// Canvas for chart, inside Results-tab content
	var resultCanvas = document.createElement("canvas");
	resultCanvas.setAttribute("class", "result-canvas");

	resultChartContainer.appendChild(resultCanvas);
	results.appendChild(resultChartContainer);

	
	//// History
	// Div for History-tab content
	var history = document.createElement("div");
	var historyId = category+"history"+place+"_pollID_"+poll.id;
	history.setAttribute("id", historyId);
	history.setAttribute("class", "tab-pane fade");
	
	// Div inside History-tab
	var historyChartContainer = document.createElement("div");
	historyChartContainer.setAttribute("class", "history-chart-container");
	
	// Canvas for chart, inside History-tab content
	var historyCanvas = document.createElement("canvas");
	historyCanvas.setAttribute("class", "history-canvas");
	
	historyChartContainer.appendChild(historyCanvas);
	history.appendChild(historyChartContainer);
	
	tabContent.appendChild(results);
	tabContent.appendChild(history);
	
	//// ANSWER TAB
	// Answer form
	var formDiv = document.createElement('div');
	formDiv.setAttribute('id', category+'answer'+place+"_pollID_"+poll.id);
	formDiv.setAttribute('class', 'tab-pane fade');
	
	var form = document.createElement("form");
	
	form.setAttribute("class", "pollAnswerForm");
	
	// Creates radiobuttons for answers
	var replyOptions = poll.replyOptions;
	var replyLabels = [];
	for (var i = 0; i < replyOptions.length; i++) {
		replyLabels.push(replyOptions[i].replyOption);
		
		var id = replyOptions[i].id;
		var input = document.createElement("input");
		if (i < 1) {
			input.checked = true;
		}
		input.setAttribute("type", "radio");
		input.setAttribute("name", "vote");
		input.setAttribute("value", id);
		
		var br = document.createElement("br");
		var label = document.createElement("label");
		label.setAttribute("for", id);
		label.textContent = replyOptions[i].replyOption;
		form.appendChild(input);
		form.appendChild(label);
		form.appendChild(br);
	}
	
	$(form).off('submit').on('submit', function(e) {
		e.preventDefault();
		var form = this;
		
		// HIDDEN INPUT FIELD FOR POLL.ID  
		var hiddenInput = document.createElement("input");
		hiddenInput.setAttribute("name", "pollId");
		hiddenInput.setAttribute("value", poll.id);
		hiddenInput.setAttribute("hidden", "hidden");
		
		form.appendChild(hiddenInput);
		
		var serializedData = $(this).serialize();
		console.log(serializedData);
		$.ajax({
			url: "php/pdo.php",
			type: "post",
			dataType: "json",
			data: serializedData,
			success: updateCharts
		});
		var children = form.children;
		for (var i = 0; i < children.length; i++) {
			console.log(children[i].getAttribute("type"));
			if (children[i].type == "submit") {
				children[i].setAttribute("disabled", true);
			}
		}
		/**
		 * Updates result chart for now
		 * Is run when pressing submit button
		**/
		function updateCharts(reply) {

			 var poll = reply[0];
			 pollDetails.innerHTML = "Poll ID: " + poll.id + "<br>Vote count: " + poll.voteCount + "<br>Created: " + poll.date;
			 $( resultCanvas ).replaceWith( drawResultPiechart(poll, resultCanvas));
			 $( historyCanvas ).replaceWith( drawHistoryChart(poll, historyCanvas));
			 
			 $(tabList).find("a:first").tab("show");
			//form.querySelector("[type=submit]").disabled = true;
		}
	});
	
	
	// Submit button for answer form
	var submitButton = document.createElement("input");
	submitButton.setAttribute("type", "submit");
	submitButton.setAttribute("value", "Submit");
	submitButton.setAttribute("class", "btn btn-primary active");
	/*submitButton.setAttribute("onsubmit", function(event) {
		console.log("submit");
		var button = event.currentTarget;
		var serializedData = $(button).closest("form").serializeArray();
		console.log(button);
		console.log(serializedData);
		
		return false;
	});*/
	form.appendChild(submitButton);
	
	formDiv.appendChild(form);
	tabContent.appendChild(formDiv);
	
	pollContainer.appendChild(tabContent);
//	pollContainer.appendChild(form);
	container.appendChild(pollContainer);
	
	//// Creating chart
//		var canvasesQuery = category + " #" + resultsId + " #result-canvas";

//	var canvasesQuery = "#" + category + " #" + resultsId + " #result-canvas";
	
	drawResultPiechart(poll, resultCanvas);
	drawHistoryChart(poll, historyCanvas);
	
	if ( $("#toggleswitch").hasClass("switch-active")) { //should show results
		$('.nav-tabs li.results').children('a').tab('show');

	} else { //should show answer forms
		$('.nav-tabs li.answer').children('a').tab('show');
	}

	// {console.log(canvasesQuery);
	//var canvases = document.querySelectorAll(canvasesQuery);
	// var canvases = document.getElementById(category).getElementsByTagName("canvas");
	// for (var i = 0; i < canvases.length; i++) {
	// 	var ctx = canvases[i].getContext("2d");
	// 	var backgroundColors = ["#24d845", "#3498db", "#f1c40f", "#c6240d", "#9b59b6", "#e74c3c"];
	// 	var replyData = [];
	// 	var replyColors = [];
	// 	for (var k = 0; k < replyOptions.length; k++) {
	// 		replyData.push(replyOptions[k].voteCount);
	// 		replyColors.push(backgroundColors[k]);
	// 	}
	// 	var dataWithNoReplys = 0;
	// 	for (var k = 0; k < replyData.length; k++) {
	// 		if (replyData[k] < 1) {
	// 			dataWithNoReplys++;
	// 		}
	// 	}
	// 	// Checks if the poll has any votes yet
	// 	if (dataWithNoReplys < replyData.length) {
	// 		// Creates a chart if there are votes
	// 		var resultChart = new Chart(ctx, {
	// 			type: 'pie',
	// 			data: {
	// 				labels: replyLabels,
	// 				datasets: [{
	// 					backgroundColor: replyColors,
	// 					data: replyData
	// 				}]
	// 			},
	// 			options: {
	// 		        legend: {
	// 		            display: true,
	// 		            labels: {
	// 		                fontSize: 16,
	// 		                fontColor: "#3a3a37"
	// 		            }
	// 		        },
	// 		        tooltips: {
	// 		        	bodyFontSize: 13,
	// 		        }
	// 			}
	// 		});
	// 	// Creates a placeholder if there are no votes
	// 	} else {
	// 		var parent = canvases[i].parentElement;
	// 		var p = document.createElement("p");
	// 		p.textContent = "No votes yet.";
	// 		parent.appendChild(p);
	// 		parent.removeChild(resultCanvas);
	// 	}
	// }
	//}
}
/**
 * Handles drawing for result chart
 * @param poll - JSON that contains data of the poll
 * @param resultCanvas - Result canvas element
**/
function drawResultPiechart(poll, resultCanvas){

		var ctx = resultCanvas.getContext("2d");
		var backgroundColors = ["#24d845", "#3498db", "#f1c40f", "#c6240d", "#9b59b6", "#e74c3c"];
		var replyData = [];
		var replyLabels = [];
		var replyColors = [];
		
		// Checks if the poll has any votes yet
		if (poll.voteCount > 0) {

			// Creates a chart only if there are votes
			for (var k = 0; k < poll.replyOptions.length; k++) {
			replyData.push(poll.replyOptions[k].voteDates.length);
			replyLabels.push(poll.replyOptions[k].replyOption);
			replyColors.push(backgroundColors[k]);
				}

			var resultChart = new Chart(ctx, {
				type: 'pie',
				data: {
					labels: replyLabels,
					datasets: [{
						backgroundColor: replyColors,
						data: replyData
					}]
				},
				options: {
					legend: {
						display: true,
						labels: {
							fontSize: 16,
							fontColor: "#3a3a37"
						}
					},
					tooltips: {
						bodyFontSize: 13,
					}
				}
			});
			var parent = resultCanvas.parentElement;
			var parentChildren = parent.children;
			for (var i = 0; i < parentChildren.length; i++) {
				if (parentChildren[i].tagName == "P") {
					parent.removeChild(parentChildren[i]);
				}
			}
			
			
			return resultCanvas;
		// Creates a placeholder if there are no votes
		} else {
			var parent = resultCanvas.parentElement;
			var p = document.createElement("p");
			p.textContent = "No votes yet.";
			parent.appendChild(p);

//			parent.removeChild(resultCanvas);
		}
		
		
	
}

/**
 * Handles drawing for history chart
 * @param poll - JSON that contains data of the poll
 * @param HistoryCanvas - History canvas element
**/
function drawHistoryChart(poll, historyCanvas) {
	var ctx = historyCanvas.getContext("2d");
	var replyOptions = poll.replyOptions;
	
	// For parsing date format that MYSQL database creates
	function parseDate(string) {
		var parts = string.split(" ");
		var dateParts = parts[0].split("-");
		
		var year = parseInt(dateParts[0]);
		var month = parseInt(dateParts[1]);
		var day = parseInt(dateParts[2]);
		
		var date = {
			year: year,
			month: month,
			day: day
		}
		return date;
	}
	
	// Creates tables for inserting to line chart
	var backgroundColors = ["#24d845", "#3498db", "#f1c40f", "#c6240d", "#9b59b6", "#e74c3c"];
	var currentDateObject = new Date();
	var daysBackCheckCount = 14;

	//Create date labels for days of last 2 weeks; date = (today - 0...14 days)  
	var dates = [];
	for (var k = 0; k < daysBackCheckCount; k++) {
		var date = new Date();
		date.setDate(currentDateObject.getDate() - k);
		var dateString = date.getDate() + "." + (date.getMonth()+1) + "." + date.getFullYear();
		dates.push(dateString);
	}
	dates = dates.reverse();
	var datasets = [];
	// Counts the votes for each day
	for (var i = 0; i < replyOptions.length; i++) {
		var datesOfOption = replyOptions[i].voteDates;
		var data = [];
		for (var k = 0; k < dates.length; k++) {
			var count = 0;
			for (var j = 0; j < datesOfOption.length; j++) {
				var voteDate = parseDate(datesOfOption[j]);
				var voteDateString = voteDate.day + "." + voteDate.month + "." + voteDate.year;
				if (dates[k] == voteDateString) {
					count++;
				}
			}
			if (k > 0) {
				data.push(count+data[k-1]);
			} else {
				data.push(count);
			}
		}
		var dataset = {
			label: replyOptions[i].replyOption,
			data: data,
			backgroundColor: backgroundColors[i],
			fill: false,
			lineTension: 0,
			borderColor: backgroundColors[i]
		}
		datasets.push(dataset);
	}
	/*console.log(poll.question);
	console.log("Dates: " + dates);
	console.log("datasets: " + datasets);*/
	
	
	var historyChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: dates,
			datasets: datasets
		  }
	});
	
	return historyCanvas;
}


/** 
 * Function for searching polls with the searchbar
**/
function searchPolls() {
	var searchInput = document.getElementById('searchbar').value;
	
	if (searchInput.length < 1) 
	{
		//don't query empty strings
	} else 
	{
		$('body, html').animate({ scrollTop: 0 }, 600, "linear");
		searchInput = parseInt(document.getElementById('searchbar').value);
		var searchContainer = document.getElementById('search-container');
		searchContainer.setAttribute("style", "display:block");
		
		for (var i = 0; i < searchContainer.children.length; i++) {
			var child = searchContainer.children[i]
			if (child.tagName == "P") {
				searchContainer.removeChild(child);
			}
		}
		var searchDiv = document.getElementById('search');
		while (searchDiv.firstChild) {
			searchDiv.removeChild(searchDiv.firstChild);
		}
		try {

			insertPoll({category: "search", pollId: searchInput});
		} catch (e) {
		}
	}
}

/**
 * Function for hiding search container
**/
function searchClose() {
	document.getElementById('search-container').style.display = 'none'
	
	var searchDiv = document.getElementById('search');
	while (searchDiv.firstChild) {
		searchDiv.removeChild(searchDiv.firstChild);
	}
}