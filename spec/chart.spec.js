/* global requestJSON, $ */
describe('Chart module', function() {
    beforeAll(function() {
        var chart = (function() {
        return {
            request: function requestJSON(parameters, callback) {
	            // Requests JSON from server
            	$.ajax({
            		method: "GET",
            		url: "php/pdo.php",
            		dataType: "json",
            		data: parameters,
            		success: function(responseText) {callback(responseText)}
            	});
            }
            };
        }());
        var question = "Test question?";
        var responses = ["Response1", "Response2"];
        var poll_id;
        function setPollId(pollId) {
            poll_id = pollId;
        }
        $.ajax({
            url: "src/php/pdo.php",
            method: "post",
            datatype: "JSON",
            data: {question: question, options: responses},
            success: function(responseText) {
                setPollId(responseText.id);
            }
        });
    });
    
    describe("requestJSON()", function() {
        it("can request poll JSON from the server", function(poll_id) {
            var json;
            function setJSON(responseText) {
                json = responseText;
            }
            chart.request({category: "search", pollId: poll_id}, setJSON);
            expect((json.id).toBe(poll_id));
        });
    });
});