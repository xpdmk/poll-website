<!--Bootstrap teema: -->
https://bootswatch.com/cosmo/

<!--Bootstrap slider/switch: -->
http://www.w3schools.com/howto/howto_css_switch.asp

<!--Toggleable nav-tabs (tokavika esimerkki sivulla)-->
http://www.w3schools.com/bootstrap/bootstrap_tabs_pills.asp
http://www.w3schools.com/bootstrap/tryit.asp?filename=trybs_tabs_dynamic&stacked=h

<!--Form jQuery JSON serializeObject -->
http://stackoverflow.com/a/1186309

<!--PHP MYSQL PDO-->
https://phpbestpractices.org/#mysql

<!--Cloud9 Mysql -setup ohjeet-->
https://community.c9.io/t/setting-up-mysql/1718

<!--More-button / AJAX -->
https://www.sanwebe.com/2013/03/loading-more-results-from-database

<!--Jasmine jquery plugin -->
https://github.com/velesin/jasmine-jquery